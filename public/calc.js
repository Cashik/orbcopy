const WETHAddress = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";//change
const stakerAddress = "0xB8C63A75eFAee44574eefcE78Ff0E1A50AcD95B3";

const poolAddress = "0x1bb0216bc8d249726c83d67f6f3e30569cec83d4";
const tokenAddress = "0x7BB594b3c757801346801f025699E39E7aaf5a49";

function dataCreate(input){
    let web3 = new Web3();
    return web3.utils.sha3(input).slice(0, 10);
};

function secondsToDhms(seconds) {
    seconds = Number(seconds);
    var d = Math.floor(seconds / (3600*24));
    var h = Math.floor(seconds % (3600*24) / 3600);
    var m = Math.floor(seconds % 3600 / 60);
    var s = Math.floor(seconds % 60);
    
    var dDisplay = d > 0 ? d + (d == 1 ? " day, " : " days, ") : "";
    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute and " : " minutes and ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return dDisplay + hDisplay + mDisplay + sDisplay;
}

function toHex(input){

    var hexresult = input.toString(16);

    return "0".repeat(64 - hexresult.length) + hexresult; 
};

function myFloor(numb, pre) {

    let my = 10**pre;
    
    let it = numb*my;
    
    it = Math.floor(it)

    return (it/my).toFixed(pre);
}


function toPad(input){

    return "0".repeat(64 - input.length) + input; 
};

async function getTokenBalance(web3input, Address, tokenAddress, callback) {
                
    let minABI = [{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"type":"function"}];

    const tokenContract = new web3input.eth.Contract(minABI,tokenAddress);
    
    tokenContract.methods.balanceOf(Address).call().then(function(balance){
            
        callback(balance);
    });
};

function getJson(url, callback) {

    var Xhr = new XMLHttpRequest();
    Xhr.open('GET', url, true);
    Xhr.responseType = 'json';
    Xhr.onload = function() {callback(Xhr.response);};
    Xhr.send();
};

function writeValue(elementId, value){
    document.getElementById(elementId).innerHTML = value;
};

function newTabLink(name, url) {

    var link = "<a href=\"" + url + "\" target=\"_blank\">" + name + "</a>";
    return link;
};

function writeError(elementId){
    
    let data = "<span style ='color: red;'><a style ='color: red' href=\"" + "https://metamask.io/" + "\" target=\"_blank\">" + "Metamask" + "</a>" + " not detected.</span>";
    
    writeValue(elementId, data);
};

function numberWithCommas(x) {
    x = x.toFixed(2)
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



const day = 86400;
//const min = 60;


var myVars = [];
var myFuncs = [];

function storeVar(name, value) {

    myVars[name] = value;

    //all functions 
    printTVL();


    printAPY();
    printUniStaked();
    printRewardAmount()
    printReferralEarned()
    // printKyeUNIStaked();
    // printKyeRewardAmount();
    // printKyePrice();
    // printKyeMarketCap();
    // printReferralEarned();
    
    // printUsdAPY();
    // printUsdUNIStaked();
    // printUsdRewardAmount();
    
    // printBtcAPY();
    // printBtcUNIStaked();
    // printBtcRewardAmount();
    
    // printEthAPY();
    // printEthUNIStaked();
    // printEthRewardAmount();

};

function someMissing(name,myArray) {

    if(myFuncs[name] != undefined){return true;}

    for (i = 0; i < myArray.length; i++) {

        if(myVars[myArray[i]] == undefined){
            return true;
        }
    } 

    if(myFuncs[name] != undefined){return true;}
    myFuncs[name] = true;
    return false;
};

async function loadValues(){
    
    let ethereum = window.ethereum;
    
    if(typeof ethereum == 'undefined'){
        console.log("Metamask not detected");
        writeError("connectError");
        return;
    }
    
    
    let accounts = await ethereum.enable();
    //user said yes
    let account = accounts[0];
    let web3 = new Web3()
    web3.setProvider(ethereum);
    
    myVars = [];
    myFuncs = [];
    
    
    //referral link
    
    let link = "https://orb.bz/?r=" + account.substring(2)
    link = "<a href='" + link + "' target='_blank'>" + link + "</a>";
    
    writeValue("referralLink", link ) ;
    
    
    //eth price
    getJson("https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd", function(EthPriceData) {
        console.log("Eth Price:", EthPriceData.ethereum.usd)
        storeVar("ethPrice", EthPriceData.ethereum.usd);
    });
    
    //colateral pool amount
    // web3.eth.getBalance(liquidityPoolAddress, (er,balance) => {
    //     storeVar("liquidityPoolEthAmount", balance/Math.pow(10, 18));
    // })
    
    //kye uniswap eth amount, only used for TVL
    getTokenBalance(web3, poolAddress, WETHAddress, (balance) => {
        storeVar("poolEthAmount",balance/Math.pow(10, 18));
    })

    //APY
    let minABI = [{"inputs":[{"internalType":"uint256","name":"ethTime","type":"uint256"}],"name":"earnCalc","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    let stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.earnCalc(31557600 * 100000000 ).call().then(function(data){
        
        storeVar("APY", data);
    });
    
    //kyePrice
    minABI = [{"inputs":[],"name":"price","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.price().call().then(function(data){
        
        storeVar("orbPrice", data/Math.pow(10, 18));
    });
        
    //1
    minABI = [{"inputs":[{"internalType":"address","name":"who","type":"address"}],"name":"viewLPTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.viewLPTokenAmount(account).call().then(function(data){
        
        storeVar("LPTokenAmount", data/Math.pow(10, 18));
    });
    
    minABI = [{"inputs":[{"internalType":"address","name":"who","type":"address"}],"name":"viewRewardTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.viewRewardTokenAmount(account).call().then(function(data){
        
        
        console.log(data);
        
        storeVar("rewardAmount", data/Math.pow(10, 18));
    });
    
    minABI = [{"inputs":[{"internalType":"address","name":"who","type":"address"}],"name":"viewPooledEthAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.viewPooledEthAmount(account).call().then(function(data){
        
        storeVar("pooledEthAmount", data/Math.pow(10, 18));
    });
    
    //2
    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewLPTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,staker2Address);
    // stakerContract.methods.viewLPTokenAmount(tokens['kye'], account).call().then(function(data){
        
    //     storeVar("kyeLPTokenAmount2", data/Math.pow(10, 18));
    // });
    
    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewRewardTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,staker2Address);
    // stakerContract.methods.viewRewardTokenAmount(tokens['kye'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("kyeRewardAmount2", data/Math.pow(10, 18));
    // });
    
    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewPooledEthAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,staker2Address);
    // stakerContract.methods.viewPooledEthAmount(tokens['kye'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("kyePooledEthAmount2", data/Math.pow(10, 18));
    // });
    
    //ethearned
    minABI = [{"inputs":[{"internalType":"address","name":"who","type":"address"}],"name":"viewReferralEarned","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.viewReferralEarned(account).call().then(function(data){
        
        
        console.log(data);
        
        storeVar("referralEarned", data/Math.pow(10, 18));
    });
    
    // minABI = [{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // let tokenContract = new web3.eth.Contract(minABI,tokens['kye']);
    // tokenContract.methods.totalSupply().call().then(function(data){
        
        
    //     console.log("kyeTotalSupply", data);
        
    //     storeVar("kyeTotalSupply", data/Math.pow(10, 18));
    // });

    //////////////////////////////////////////////////////////////////////////////
    //USD

    //usd uniswap eth amount
    //apy
    //LPTokenAmount
    //RewardAmount
    //PooledEthAmount
    
    //usd uniswap eth amount, only used for TVL
    // getTokenBalance(web3, poolAddresses['usd'], WETHAddress, (balance) => {
    //     storeVar("usdPoolEthAmount",balance/Math.pow(10, 18));
    // })

    // minABI = [{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"ethTime","type":"uint256"}],"name":"earnCalc","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.earnCalc(tokens['usd'], 31557600 * 100000000 ).call().then(function(data){
        
    //     storeVar("usdAPY", data);
    // });


    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewLPTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewLPTokenAmount(tokens['usd'], account).call().then(function(data){
        
    //     storeVar("usdLPTokenAmount", data/Math.pow(10, 18));
    // });

    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewRewardTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewRewardTokenAmount(tokens['usd'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("usdRewardAmount", data/Math.pow(10, 18));
    // });

    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewPooledEthAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewPooledEthAmount(tokens['usd'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("usdPooledEthAmount", data/Math.pow(10, 18));
    // });
    
     //////////////////////////////////////////////////////////////////////////////
    //BTC

    //usd uniswap eth amount
    //apy
    //LPTokenAmount
    //RewardAmount
    //PooledEthAmount
    
    //usd uniswap eth amount, only used for TVL
    // getTokenBalance(web3, poolAddresses['btc'], WETHAddress, (balance) => {
    //     storeVar("btcPoolEthAmount",balance/Math.pow(10, 18));
    // })

    // minABI = [{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"ethTime","type":"uint256"}],"name":"earnCalc","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.earnCalc(tokens['btc'], 31557600 * 100000000 ).call().then(function(data){
        
    //     storeVar("btcAPY", data);
    // });


    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewLPTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewLPTokenAmount(tokens['btc'], account).call().then(function(data){
        
    //     storeVar("btcLPTokenAmount", data/Math.pow(10, 18));
    // });

    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewRewardTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewRewardTokenAmount(tokens['btc'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("btcRewardAmount", data/Math.pow(10, 18));
    // });

    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewPooledEthAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewPooledEthAmount(tokens['btc'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("btcPooledEthAmount", data/Math.pow(10, 18));
    // });
    
     //////////////////////////////////////////////////////////////////////////////
    //ETH

    //usd uniswap eth amount
    //apy
    //LPTokenAmount
    //RewardAmount
    //PooledEthAmount
    
    //usd uniswap eth amount, only used for TVL
    // getTokenBalance(web3, poolAddresses['eth'], WETHAddress, (balance) => {
    //     storeVar("ethPoolEthAmount",balance/Math.pow(10, 18));
    // })

    // minABI = [{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"ethTime","type":"uint256"}],"name":"earnCalc","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.earnCalc(tokens['eth'], 31557600 * 100000000 ).call().then(function(data){
        
    //     storeVar("ethAPY", data);
    // });


    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewLPTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewLPTokenAmount(tokens['eth'], account).call().then(function(data){
        
    //     storeVar("ethLPTokenAmount", data/Math.pow(10, 18));
    // });

    // minABI = [{"inputs":[{"internalType":"address","name":"tokenAddress","type":"address"},{"internalType":"address","name":"who","type":"address"}],"name":"viewRewardTokenAmount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    // stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    // stakerContract.methods.viewRewardTokenAmount(tokens['eth'], account).call().then(function(data){
        
        
    //     console.log(data);
        
    //     storeVar("ethRewardAmount", data/Math.pow(10, 18));
    // });

    minABI = [{"inputs":[{"internalType":"address","name":"who","type":"address"}],"name":"timePooled","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}];
    stakerContract = new web3.eth.Contract(minABI,stakerAddress);
    stakerContract.methods.timePooled(account).call().then(function(data){
        
        console.log("timePooled:", data);
        
        storeVar("timePooled", data);
    });



    
}

function printTVL() {

    if( someMissing( "printTVL", ["ethPrice", "poolEthAmount"]) ){return}
    
    let output = myVars["ethPrice"]*(2*(myVars["poolEthAmount"]));

    writeValue( "TVL",  "$" + numberWithCommas(output) ) 

};

function printAPY() {

    if( someMissing( "printAPY", ["APY", "orbPrice"]) ){return}

    
    let kyeAmount = myVars["orbPrice"]*myVars["APY"];
    
    let APY = ((kyeAmount + 100000000)/100000000);
    
    APY = ((APY - 1) * 100)
    
    //APY = (Math.exp(APY-1) - 1) * 100;

    writeValue( "APY",  numberWithCommas(APY) + "%" ) 

}

function printUniStaked(){
    
    if( someMissing( "printUniStaked", ["LPTokenAmount", "pooledEthAmount", "ethPrice"]) ){return}
    
    let output = myFloor(myVars["LPTokenAmount"], 8) + " ($" + numberWithCommas((2*myVars["pooledEthAmount"])*myVars["ethPrice"]) + ")"

    writeValue( "uniStaked", output)
    writeValue( "uniStaked2", output) 
}

function printRewardAmount(){
    
    if( someMissing( "printRewardAmount", ["rewardAmount", "orbPrice", "ethPrice"]) ){return}
    
    console.log(myVars["rewardAmount"])
    
    console.log("orbprice:", myVars["orbPrice"]*myVars["ethPrice"])
    
    let output = myFloor(myVars["rewardAmount"], 8) + " ($" + numberWithCommas(myVars["rewardAmount"]*myVars["orbPrice"]*myVars["ethPrice"]) + ")"

    writeValue( "rewardAmount",  output ) 
    writeValue( "rewardAmount2", output ) 
}

function printReferralEarned(){
    
    if( someMissing( "printReferralEarned", ["ethPrice", "referralEarned", "orbPrice"]) ){return}
    
    let output = myFloor(myVars["referralEarned"], 8) + " ($" + numberWithCommas(myVars["referralEarned"]*myVars["ethPrice"]*myVars["orbPrice"]) + ")"
   
    writeValue( "referralEarned",  output)
    writeValue( "referralEarned2",  output) 
}

async function stakeToken(){
    
    let ethereum = window.ethereum;
    
    if(typeof ethereum == 'undefined'){
        console.log("Metamask not detected");
        writeError("stakeError");
        return;
    }
    
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let r = urlParams.get('r')
    console.log(r);
    
    if(r == null){
        
        r = "0";
        
    }else if(r.length != 40){
        
        r = "0";
    }
    
    console.log(r.length)

    let accounts = await ethereum.enable();
    //user said yes
    let account = accounts[0];
    let web3 = new Web3()
    web3.setProvider(ethereum);
    
    let amount = document.getElementById("stakeInput").value;
    
    let data = dataCreate("stake(address,address)") + toPad(account.substr(2)) + toPad(r);
   
    console.log('now');
    web3.eth.sendTransaction({from: account, to: stakerAddress, data: data, value: Math.round(amount*10**18) });
}

async function withdrawUni(){
    
    let ethereum = window.ethereum;
    
    if(typeof ethereum == 'undefined'){
        console.log("Metamask not detected");
        writeError("withdrawError");
        return;
    }
    
    
    let accounts = await ethereum.enable();
    //user said yes
    let account = accounts[0];
    let web3 = new Web3()
    web3.setProvider(ethereum);
    
    let amount = document.getElementById("withdrawUni").value;

    if(parseInt(myVars["timePooled"])  + 3*day > Math.floor(Date.now() / 1000) ){
        
        let secondsLeft = (parseInt(myVars["timePooled"])  + 3*day) - Math.floor(Date.now() / 1000);
        writeValue( "withdrawError", "Your lock period will be lifted in " + secondsToDhms(secondsLeft) )
        return;
    }
    
    
    
    //if(amount > 0 && amount <= myVars[ token + "RewardAmount"]){
        
        // writeValue("withdrawError", "<span style ='color: red;'>Lock period has not passed</span>")
        // return;
    //}
    
    let data = dataCreate("withdrawLPTokens(uint256)") + toHex(Math.floor(amount*10**18));
    web3.eth.sendTransaction({from: account, to: stakerAddress, data: data });
}

async function withdrawReward(){
    
    let ethereum = window.ethereum;
    
    if(typeof ethereum == 'undefined'){
        console.log("Metamask not detected");
        writeError("withdrawError");
        return;
    }
    
    
    let accounts = await ethereum.enable();
    //user said yes
    let account = accounts[0];
    let web3 = new Web3()
    web3.setProvider(ethereum);
    
    let amount = document.getElementById("withdrawStaking").value;
    
    if(parseInt(myVars["timePooled"])  + 3*day > Math.floor(Date.now() / 1000) ){
        
        let secondsLeft = (parseInt(myVars["timePooled"])  + 3*day) - Math.floor(Date.now() / 1000);
        writeValue( "withdrawError", "Your lock period will be lifted in " + secondsToDhms(secondsLeft) )
        return;
    }
    
    //if(amount > 0 && amount <= myVars[ token + "RewardAmount"]){
        
        // writeValue("withdrawError", "<span style ='color: red;'>Lock period has not passed</span>")
        // return;
    //}
    
    let data = dataCreate("withdrawRewardTokens(uint256)") + toHex(Math.floor(amount*10**18));
    web3.eth.sendTransaction({from: account, to: stakerAddress, data: data });
}

async function withdrawReferral(){
    
    let ethereum = window.ethereum;
    
    if(typeof ethereum == 'undefined'){
        console.log("Metamask not detected");
        writeError("withdrawError");
        return;
    }
    
    
    let accounts = await ethereum.enable();
    //user said yes
    let account = accounts[0];
    let web3 = new Web3()
    web3.setProvider(ethereum);
    
    let amount = document.getElementById("withdrawReferral").value;
    
    //if(parseInt(0) == 0 && parseFloat(myVars["referralEarned"]) > 0){
    if(parseInt(myVars["timePooled"]) == 0 && parseFloat(myVars["referralEarned"]) > 0){
        
        writeValue( "withdrawError",  "You need to stake something before you can withdraw referral rewards.")
        return;
    }
    
    if(parseInt(myVars["timePooled"])  + 3*day > Math.floor(Date.now() / 1000) ){
        
        let secondsLeft = (parseInt(myVars["timePooled"])  + 3*day) - Math.floor(Date.now() / 1000);
        writeValue( "withdrawError", "Your lock period will be lifted in " + secondsToDhms(secondsLeft) )
        return;
    }
   
    //Math.floor(Date.now() / 1000)
    
    // if(myVars["timePooled"] == 0 && myVars["referralEarned"] > 0){
        
        
    //     return;
    // }

    
    //if(amount > 0 && amount <= myVars[ token + "RewardAmount"]){
        
        // writeValue("withdrawError", "<span style ='color: red;'>Lock period has not passed</span>")
        // return;
    //}
    
    let data = dataCreate("withdrawReferralEarned(uint256)") + toHex(Math.floor(amount*10**18));
    web3.eth.sendTransaction({from: account, to: stakerAddress, data: data });
}

