// index.js
const path = require('path')
const express = require('express')
const exphbs = require('express-handlebars')
const hbs = require("hbs");

const app = express()

app.set("view engine", "hbs");
app.set('views', path.join(__dirname, 'views'))
app.use(express.static('public'));

app.get('/', (request, response) => {
    response.render('home', {
    })
})

app.listen(3001)